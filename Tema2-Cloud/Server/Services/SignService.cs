using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Server
{
    public class SignService : Sign.SignBase
    {
       
        public override Task<SignReply> SaySign(SignRequest request, ServerCallContext context)
        {
            bool test=false;

            Console.WriteLine("\n\n\nBirthday: " + request.Birthday + ".");

            string[] dateParts = request.Birthday.Split('/');
            DateTime birthday = new DateTime(Convert.ToInt32(dateParts[2]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[0]));
            

            DateTime aquariusDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 01, 20);
            DateTime aquariusDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 02, 18);
            DateTime piscesDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 02, 19);
            DateTime piscesDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 03, 20);
            DateTime ariesDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 03, 21);
            DateTime ariesDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 04, 19);
            DateTime taurusDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 04, 20);
            DateTime taurusDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 05, 20);
            DateTime geminiDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 05, 21);
            DateTime geminiDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 06, 20);
            DateTime cancerDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 06, 21);
            DateTime cancerDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 07, 22);
            DateTime leoDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 07, 23);
            DateTime leoDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 08, 22);
            DateTime virgoDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 08, 23);
            DateTime virgoDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 09, 22);
            DateTime libraDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 09, 23);
            DateTime libraDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 10, 22);
            DateTime scorpioDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 10, 23);
            DateTime scorpioDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 11, 21);
            DateTime sagittariusDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 11, 22);
            DateTime sagittariusDate2 = new DateTime(Convert.ToInt32(dateParts[2]), 12, 21);
            DateTime capricornDate1 = new DateTime(Convert.ToInt32(dateParts[2]), 12, 22);
            DateTime capricornDate2 = new DateTime(Convert.ToInt32(dateParts[2]) + 1, 01, 19);


            if (DateTime.Compare(birthday, aquariusDate1) >= 0 && DateTime.Compare(birthday, aquariusDate2) <= 0)
            {
                test = true;
                Console.WriteLine("\nSign: Aquarius. \n\n\n");
            }
            else if (DateTime.Compare(birthday, piscesDate1) >= 0 && DateTime.Compare(birthday, piscesDate2) <= 0)
            {
                Console.WriteLine("\nSign: Pisces. \n\n\n");
            }
            else if (DateTime.Compare(birthday, ariesDate1) >= 0 && DateTime.Compare(birthday, ariesDate2) <= 0)
            {
                Console.WriteLine("\nSign: Aries. \n\n\n");
            }           
            else if (DateTime.Compare(birthday, taurusDate1) >= 0 && DateTime.Compare(birthday, taurusDate2) <= 0)
            {
                Console.WriteLine("\nSign: Taurus. \n\n\n");
            }          
            else if (DateTime.Compare(birthday, geminiDate1) >= 0 && DateTime.Compare(birthday, geminiDate2) <= 0)
            {
                Console.WriteLine("\nSign: Gemini. \n\n\n");
            }          
            else if (DateTime.Compare(birthday, cancerDate1) >= 0 && DateTime.Compare(birthday, cancerDate2) <= 0)
            {
                Console.WriteLine("\nSign: Cancer. \n\n\n");
            }          
            else if (DateTime.Compare(birthday, leoDate1) >= 0 && DateTime.Compare(birthday, leoDate2) <= 0)
            {
                Console.WriteLine("\nSign: Leo. \n\n\n");
            }           
            else if (DateTime.Compare(birthday, virgoDate1) >= 0 && DateTime.Compare(birthday, virgoDate2) <= 0)
            {
                Console.WriteLine("\nSign: Virgo. \n\n\n");
            }         
            else if (DateTime.Compare(birthday, libraDate1) >= 0 && DateTime.Compare(birthday, libraDate2) <= 0)
            {
                Console.WriteLine("\nSign: Libra. \n\n\n");
            }         
            else if (DateTime.Compare(birthday, scorpioDate1) >= 0 && DateTime.Compare(birthday, scorpioDate2) <= 0)
            {
                Console.WriteLine("\nSign: Scorpio. \n\n\n");
            }          
            else if (DateTime.Compare(birthday, sagittariusDate1) >= 0 && DateTime.Compare(birthday, sagittariusDate2) <= 0)
            {
                Console.WriteLine("\nSign: Sagittarius. \n\n\n");
            }          
            else if (DateTime.Compare(birthday, capricornDate1) >= 0 && DateTime.Compare(birthday, capricornDate2) <= 0)
            {
                Console.WriteLine("\nSign: Capricorn. \n\n\n");
            }


            return Task.FromResult(new SignReply
            {
                Message = "\nYour sign is on server!\n"
            }) ;
        }
    }
}
