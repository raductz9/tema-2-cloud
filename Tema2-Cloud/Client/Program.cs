﻿using Grpc.Net.Client;
using Server;
using System;
using System.Threading.Tasks;

  
namespace Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            int ok = 0;
            Console.WriteLine(" --> Enter your birthday:\n");
            string birthday = Console.ReadLine();

            while (ok!=1)
            {
                try
                {
                    string[] dateParts = birthday.Split('/');
                    DateTime testDate = new DateTime(Convert.ToInt32(dateParts[2]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[0]));
                    ok = 1;
                    Console.WriteLine("\n --> Birthday is valid!");
                }
                catch
                {
                    ok = 0;
                    Console.WriteLine("\n --> Birthday is not valid!");
                    Console.WriteLine(" --> Enter your birthday:\n");
                    birthday = Console.ReadLine();
                }
            }
           
            var input = new SignRequest { Birthday = birthday };
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Sign.SignClient(channel);

            var reply = await client.SaySignAsync(input);
            Console.WriteLine(reply.Message);
        }

    }
}
